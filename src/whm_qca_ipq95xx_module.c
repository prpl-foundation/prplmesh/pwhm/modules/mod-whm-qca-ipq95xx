/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <debug/sahtrace.h>

#include "wld/wld.h"
#include "wld/wld_radio.h"
#include "wld/wld_nl80211_api.h"

#include "whm_qca_ipq95xx_module.h"
#include "whm_qca_ipq95xx_hostapd_cfgFile.h"

#define ME "qcaMod"

static bool s_init = false;
static vendor_t* s_vendor = NULL;


swl_rc_ne whm_qca_ipq95xx_module_addRadios(void) {
    swl_rc_ne rc = SWL_RC_INVALID_PARAM;
    ASSERT_NOT_NULL(s_vendor, SWL_RC_INVALID_PARAM, ME, "NULL");
    wld_nl80211_ifaceInfo_t wlIfacesInfo[MAXNROF_RADIO][MAXNROF_ACCESSPOINT] = {};
    rc = wld_nl80211_getInterfaces(MAXNROF_RADIO, MAXNROF_ACCESSPOINT, wlIfacesInfo);
    ASSERT_FALSE(rc < SWL_RC_OK, rc, ME, "fail to get nl80211 interfaces");
    uint8_t index = 0;
    for(uint32_t i = 0; i < MAXNROF_RADIO; i++) {
        wld_nl80211_ifaceInfo_t* pMainIface = &wlIfacesInfo[i][0];
        if(pMainIface->ifIndex <= 0) {
            continue;
        }
        if(wld_rad_get_radio(pMainIface->name) != NULL) {
            index++;
            continue;
        }
        SAH_TRACEZ_WARNING(ME, "Interface %s attached to vendor %s", pMainIface->name, s_vendor->name);
        wld_addRadio(pMainIface->name, s_vendor, index);
        index++;
    }
    if(index == 0) {
        SAH_TRACEZ_INFO(ME, "NO Wireless interface found");
        return SWL_RC_ERROR;
    }

    return SWL_RC_OK;
}

bool whm_qca_ipq95xx_module_init(void) {
    ASSERT_FALSE(s_init, false, ME, "already initialized");
    SAH_TRACEZ_INFO(ME, "qca ipq95xx init");

    const T_CWLD_FUNC_TABLE* nl80211Fta = wld_nl80211_getVendorTable();
    ASSERT_NOT_NULL(nl80211Fta, false, ME, "nl80211 FTA is not initiated");

    T_CWLD_FUNC_TABLE fta;
    memset(&fta, 0, sizeof(T_CWLD_FUNC_TABLE));

    /* copy the same handlers table of default nl80211 implementation. */
    memcpy(&fta, nl80211Fta, sizeof(T_CWLD_FUNC_TABLE));

    /* customize handlers table by overwriting some specific API */
    fta.mfn_wrad_updateConfigMap = whm_qca_ipq95xx_updateConfigMap;

    /* load private vendor datamodel extension and config options*/
    swl_rc_ne ret = wld_vendorModule_parseOdl(ODL_MAIN_FILE);
    ASSERT_EQUALS(ret, SWL_RC_OK, false, ME, "Fail to load vendor odl file");

    /* register private trace zones to pwhm */
    wld_vendorModule_loadPrivTraceZones(amxo_parser_get_config(get_wld_plugin_parser(), "mod-trace-zones"));

    /* register vendor */
    s_vendor = wld_registerVendor(QCA_IPQ95XX_VENDOR_NAME, &fta);
    ASSERT_NOT_NULL(s_vendor, false, ME, "fail to register vendor %s", QCA_IPQ95XX_VENDOR_NAME);

    /* share the same generic and native fsm manager, of nl80211 wld implementation */
    wld_fsm_init(s_vendor, (wld_fsmMngr_t*) wld_nl80211_getFsmMngr());

    /* init done */
    s_init = true;

    /* detect and attach radios to newly created vendor. */
    ret = whm_qca_ipq95xx_module_addRadios();
    ASSERTW_EQUALS(ret, SWL_RC_OK, true, ME, "No radios added for vendor %s", QCA_IPQ95XX_VENDOR_NAME);

    return true;
}

bool whm_qca_ipq95xx_module_deInit(void) {
    ASSERTS_TRUE(s_init, false, ME, "Not initialized");
    ASSERTS_FALSE(wld_isVendorUsed(s_vendor), false, ME, "Still used");
    ASSERTS_TRUE(wld_unregisterVendor(s_vendor), false, ME, "unregister failure");
    s_init = false;
    s_vendor = NULL;
    return true;
}

bool whm_qca_ipq95xx_module_loadDefaults(void) {
    ASSERTS_TRUE(s_init, false, ME, "Not initialized");
    swl_rc_ne ret = wld_vendorModule_loadOdls(ODL_DEFAULTS_DIR);
    ASSERT_EQUALS(ret, SWL_RC_OK, false, ME, "Fail to load vendor odl defaults");
    return true;
}

amxd_status_t _whm_qca_ipq95xx_module_setDebugEnable_pwf(amxd_object_t* object,
                                                         amxd_param_t* parameter _UNUSED,
                                                         amxd_action_t reason _UNUSED,
                                                         const amxc_var_t* const args _UNUSED,
                                                         amxc_var_t* const retval _UNUSED,
                                                         void* priv _UNUSED) {

    amxd_status_t rv = amxd_action_param_write(object, parameter, reason, args, retval, priv);
    ASSERT_EQUALS(rv, amxd_status_ok, rv, ME, "STATUS NOK");
    /*
     * retrieve parent "Radio" object 3 levels upper "Debug"
     * WiFi.Radio.Vendor.IPQ95XX.Debug.
     */
    amxd_object_t* wifiRadObj = amxd_object_get_parent(amxd_object_get_parent(amxd_object_get_parent(object)));
    ASSERT_NOT_NULL(wifiRadObj, amxd_status_ok, ME, "NULL");
    T_Radio* pR = (T_Radio*) wifiRadObj->priv;
    ASSERT_NOT_NULL(pR, amxd_status_ok, ME, "NULL");
    SAH_TRACEZ_IN(ME);
    bool flag = amxc_var_dyncast(bool, args);
    SAH_TRACEZ_INFO(ME, "%s: %s: set Debug Enable - %d", pR->Name, pR->vendor->name, flag);

    SAH_TRACEZ_OUT(ME);
    return rv;
}

amxd_status_t _whm_qca_ipq95xx_module_setDebugLevel_pwf(amxd_object_t* object,
                                                        amxd_param_t* parameter _UNUSED,
                                                        amxd_action_t reason _UNUSED,
                                                        const amxc_var_t* const args _UNUSED,
                                                        amxc_var_t* const retval _UNUSED,
                                                        void* priv _UNUSED) {

    amxd_status_t rv = amxd_action_param_write(object, parameter, reason, args, retval, priv);
    ASSERT_EQUALS(rv, amxd_status_ok, rv, ME, "STATUS NOK");
    /*
     * retrieve parent "Radio" object 3 levels upper "Debug"
     * WiFi.Radio.Vendor.IPQ95XX.Debug.
     */
    amxd_object_t* wifiRadObj = amxd_object_get_parent(amxd_object_get_parent(amxd_object_get_parent(object)));
    ASSERT_NOT_NULL(wifiRadObj, amxd_status_ok, ME, "NULL");
    T_Radio* pR = (T_Radio*) wifiRadObj->priv;
    ASSERT_NOT_NULL(pR, amxd_status_ok, ME, "NULL");
    SAH_TRACEZ_IN(ME);
    uint32_t level = amxc_var_dyncast(uint32_t, args);
    SAH_TRACEZ_INFO(ME, "%s: %s: set Debug Level - %d", pR->Name, pR->vendor->name, level);

    SAH_TRACEZ_OUT(ME);
    return rv;
}

